#Change $ListFile to Correct file name


$answer = Read-Host -Prompt "Clear the list (y/n)?";
While (($answer -ne "y") -And ($answer -ne "n")) {
$answer = Read-Host -Prompt "Invalid input, please enter y or n";
}
If ($answer -Eq 'y') {
Set-Content -path $ListFile -value $null;
}
